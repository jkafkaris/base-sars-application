package com.kafkaris.example.views {
	import robotlegs.bender.bundles.mvcs.Mediator;

	import com.kafkaris.example.signals.SetupSignal;

	public class MainApplicationMediator extends Mediator {
		[Inject]
		public var view:MainApplicationView;

		[Inject]
		public var performSetup:SetupSignal;

		public function MainApplicationMediator() {
		}

		override public function initialize():void {
			super.initialize();
			performSetup.dispatch();
		}
	}
}