package com.kafkaris.example.views.button {
	import robotlegs.bender.bundles.mvcs.Mediator;
	import robotlegs.bender.framework.api.ILogger;

	import com.kafkaris.example.signals.ButtonActivated;

	public class ButtonMediator extends Mediator {
		[Inject]
		public var view:IButton;

		[Inject]
		public var logger:ILogger;

		[Inject]
		public var buttonActivated:ButtonActivated;

		public function ButtonMediator() {
		}

		override public function initialize():void {
			super.initialize();
			view.triggered.add(onTriggered);
		}

		private function onTriggered(message:String):void {
			logger.debug(message);
			buttonActivated.dispatch(view, "JONO", 2);
		}
	}
}