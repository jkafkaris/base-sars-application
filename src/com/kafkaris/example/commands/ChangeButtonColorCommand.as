package com.kafkaris.example.commands {
	import robotlegs.bender.bundles.mvcs.Command;

	import com.kafkaris.example.views.button.IButton;

	public class ChangeButtonColorCommand extends Command {
		[Inject]
		public var button:IButton;

		[Inject]
		public var string1:String;

		[Inject]
		public var string2:int;

		public function ChangeButtonColorCommand() {
		}

		override public function execute():void {
			button.updateColor(Math.random() * 0xFFFFFF);
		}
	}
}