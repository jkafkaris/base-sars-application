package com.kafkaris.example {
	import away3d.cameras.Camera3D;
	import away3d.containers.Scene3D;
	import away3d.containers.View3D;
	import away3d.controllers.HoverController;
	import away3d.core.managers.Stage3DManager;
	import away3d.core.managers.Stage3DProxy;
	import away3d.debug.AwayStats;
	import away3d.debug.Trident;
	import away3d.events.Stage3DEvent;

	import robotlegs.bender.bundles.SARSBundle;
	import robotlegs.bender.extensions.contextView.ContextView;
	import robotlegs.bender.extensions.sarsIntegration.api.StarlingCollection;
	import robotlegs.bender.framework.impl.Context;

	import starling.core.Starling;

	import com.kafkaris.example.config.Config;
	import com.kafkaris.example.views.MainApplicationView;

	import flash.display.Sprite;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;

	/**
	 * @author Jono Kafkaris
	 */
	[SWF(backgroundColor="#FFFFFF", frameRate="60", width="800", height="600")]
	public class BaseApplication extends Sprite {
		private var _stage3DManager:Stage3DManager;
		private var _stage3DProxy:Stage3DProxy;
		private var _scene:Scene3D;
		private var _camera:Camera3D;
		private var _view:View3D;
		private var _cameraController:HoverController;
		private var _stats:AwayStats;
		private var _trident:Trident;
		private var _starlingUI:Starling;
		private var _starlingBackground:Starling;
		private var _context:Context;

		public function BaseApplication() {
			super();
			addEventListener(Event.ADDED_TO_STAGE, addedToStageHandler);
		}

		private function addedToStageHandler(event:Event):void {
			stage.align = StageAlign.TOP_LEFT;
			stage.scaleMode = StageScaleMode.NO_SCALE;

			_stage3DManager = Stage3DManager.getInstance(stage);
			_stage3DProxy = _stage3DManager.getFreeStage3DProxy();
			_stage3DProxy.addEventListener(Stage3DEvent.CONTEXT3D_CREATED, context3DCreatedHandler);
		}

		private function context3DCreatedHandler(event:Stage3DEvent):void {
			initAway3D();
			initStarling();

			_context = new Context();

			var starlingCollection:StarlingCollection = new StarlingCollection();
			starlingCollection.addItem(_starlingBackground, "background");
			starlingCollection.addItem(_starlingUI, "ui");

			_context.install(SARSBundle).configure(_view, starlingCollection, Config, new ContextView(this));

			stage.addEventListener(Event.RESIZE, resizeHandler, false, 0, true);
			stage.addEventListener(Event.ENTER_FRAME, enterFrameHandler, false, 0, true);
		}

		private function initStarling():void {
			_starlingUI = new Starling(MainApplicationView, stage, _stage3DProxy.viewPort, _stage3DProxy.stage3D);
			_starlingUI.enableErrorChecking = false;
			_starlingUI.start();

			_starlingBackground = new Starling(MainApplicationView, stage, _stage3DProxy.viewPort, _stage3DProxy.stage3D);
			_starlingBackground.enableErrorChecking = false;
			_starlingBackground.start();
		}

		private function initAway3D():void {
			_scene = new Scene3D();

			_camera = new Camera3D();
			_camera.z = -600;
			_camera.y = 500;

			_view = new View3D();
			_view.camera = _camera;
			_view.scene = _scene;
			_view.stage3DProxy = _stage3DProxy;
			_view.shareContext = true;
			addChild(_view);

			_cameraController = new HoverController(_camera, null, 150, 10, 2000);

			_stats = new AwayStats(_view, true);
			_stats.mouseEnabled = false;
			_stats.x = 5;
			_stats.y = 5;
			this.addChild(_stats);

			_trident = new Trident();
			_trident.mouseEnabled = false;
			_view.scene.addChild(_trident);
		}

		private function resizeHandler(event:Event = null):void {
			_view.width = stage.stageWidth;
			_view.height = stage.stageHeight;
		}

		private function enterFrameHandler(event:Event):void {
			_stage3DProxy.clear();
			_starlingBackground.nextFrame();
			_view.render();
			_starlingUI.nextFrame();
			_stage3DProxy.present();
		}
	}
}
