package com.kafkaris.example.config {
	import robotlegs.bender.extensions.mediatorMap.api.IMediatorMap;
	import robotlegs.bender.extensions.signalCommandMap.api.ISignalCommandMap;
	import robotlegs.bender.framework.api.IContext;
	import robotlegs.bender.framework.api.IInjector;
	import robotlegs.bender.framework.api.LogLevel;

	import com.kafkaris.example.commands.ChangeButtonColorCommand;
	import com.kafkaris.example.commands.SetupCommand;
	import com.kafkaris.example.signals.ButtonActivated;
	import com.kafkaris.example.signals.SetupSignal;
	import com.kafkaris.example.views.MainApplicationMediator;
	import com.kafkaris.example.views.MainApplicationView;
	import com.kafkaris.example.views.button.ButtonMediator;
	import com.kafkaris.example.views.button.IButton;

	public class Config {
		[Inject]
		public var context:IContext;

		[Inject]
		public var mediatorMap:IMediatorMap;

		[Inject]
		public var commandMap:ISignalCommandMap;

		[Inject]
		public var injector:IInjector;

		public function Config() {
		}

		[PostConstruct]
		public function init():void {
			injector.fallbackProvider = new DefaultFallbackProvider();

			context.logLevel = LogLevel.INFO;

			injector.map(SetupSignal).asSingleton();
			injector.map(ButtonActivated).asSingleton();

			commandMap.map(SetupSignal).toCommand(SetupCommand);
			commandMap.map(ButtonActivated).toCommand(ChangeButtonColorCommand);

			mediatorMap.map(IButton).toMediator(ButtonMediator);
			mediatorMap.map(MainApplicationView).toMediator(MainApplicationMediator);
		}
	}
}