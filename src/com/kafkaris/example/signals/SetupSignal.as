package com.kafkaris.example.signals {
	import org.osflash.signals.Signal;

	public class SetupSignal extends Signal {
		public function SetupSignal(...parameters) {
			super(parameters);
		}
	}
}