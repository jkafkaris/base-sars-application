package com.kafkaris.example.signals {
	import com.kafkaris.example.views.button.IButton;

	import org.osflash.signals.Signal;

	public class ButtonActivated extends Signal {
		public function ButtonActivated() {
			super(IButton, String, int);
		}
	}
}