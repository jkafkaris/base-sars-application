This is an Adobe AIR project that uses the SARS (Starling/Away3D/Robotlegs/Signals https://github.com/Vj3k0/robotlegs-extensions-SARS)  example project with changes done to run the latest of each of it's components.

Currently this example contains:

* Starling 1.8
* Away3D 4.1.6
* Robotlegs 2.2.1
* Signals 0.9

Also has:

* Feathers 2.3.0

Contains the last updated version of SignalsCommandMap

Tested with Apache Flex 4.15.0 and AIR 20

This project was setup using FDT